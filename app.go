package main

import (
	"fmt"
	"net/http"
	"io"
	"io/ioutil"
	"encoding/json"
	"bytes"
)

var host = ""
var resource = ""
var resourceId = ""

func exampleGetRequest(id string) string{
	if resource == "" {
		return "Please set a resource."
	}

	resp, err := http.Get(host + "/" + resource + "/" + id)
	if err != nil {
		return err.Error()
	}

	defer resp.Body.Close()	

	body := readResponseBody(resp.Body)

	return body
}

func examplePostRequest() string{
	if resource == "" {
		return "Please set a resource."
	}

	requestBody, err := json.Marshal(map[string]string{
		"name": "data", // Data goes here. Need to read into how Go handles JSON
	})
	if err != nil {
		return err.Error()
	}

	resp, err := http.Post(host + "/" + resource, "application/json", bytes.NewBuffer(requestBody))
	if err != nil {
		return err.Error()
	}
	
	defer resp.Body.Close()	

	body := readResponseBody(resp.Body)

	return body
}

func readResponseBody(responseBody io.Reader) string{
	body, err := ioutil.ReadAll(responseBody)
	if err != nil {
		return err.Error()
	}
	return string(body)
}

func main() {
	if host != "" {
		result := exampleGetRequest(resourceId)
		fmt.Println("Result of GET request: ", result)

		result = examplePostRequest()
		fmt.Println("Result of POST request: ", result)
	} else {
		fmt.Println("Please set a host.")
	}
}